function highlightPage(){
	if(!document.getElementsByTagName) return false;
	if(!document.getElementById) return false;

	var headers = document.getElementsByTagName('header');
	if(headers.length == 0) return false;
	var navs = headers[0].getElementsByTagName('nav');
	if(navs.length == 0) return false;
	var links = navs[0].getElementsByTagName('a');
	var link_url;
	for(var i=0;i<links.length;i++){
		link_url = links[i].getAttribute('href');
		if(window.location.href.indexOf(link_url)!=-1){
			links[i].className = 'current';
			var link_text = links[i].lastChild.nodeValue.toLowerCase();
			document.body.setAttribute('id',link_text);
		}
	}
}

Racer.addLoadEvent(highlightPage);

function prepareSlideshow () {
	if(!document.getElementsByTagName) return false;
	if(!document.getElementById) return false;
	if(!document.getElementById('intro')) return false;
	
	var intro = document.getElementById('intro');
	var slideshow = document.createElement('div');
	slideshow.setAttribute('id','slideshow');

	var frame = document.createElement('img');
	frame.setAttribute('src','images/frame.gif');
	frame.setAttribute('alt','');
	frame.setAttribute('id','frame');
	slideshow.appendChild(frame);

	var preview = document.createElement('img');
	preview.setAttribute('alt','a glimpse of what awaits you');
	preview.setAttribute('src','images/slideshow.gif');
	preview.setAttribute('id','preview');
	slideshow.appendChild(preview);
	Racer.DOMHelper.insertAfter(slideshow,intro);

	var destination;
	var links = document.getElementsByTagName('a');
	for(var i=0;i<links.length;i++){
		links[i].onmouseover = function(){
			destination=this.getAttribute('href');
			if(destination.indexOf('index.html')!=-1){
				Racer.Automation.moveElement('preview',0,0,10);
			}
			if(destination.indexOf('about.html')!=-1){
				Racer.Automation.moveElement('preview',-150,0,10);
			}
			if(destination.indexOf('photos.html')!=-1){
				Racer.Automation.moveElement('preview',-300,0,10);
			}
			if(destination.indexOf('live.html')!=-1){
				Racer.Automation.moveElement('preview',-450,0,10);
			}
			if(destination.indexOf('contact.html')!=-1){
				Racer.Automation.moveElement('preview',-600,0,10);
			}
		}
	}
}

Racer.addLoadEvent(prepareSlideshow);


function showSection (id) {
	var sections = document.getElementsByTagName('section');
	for(var i=0;i<sections.length;i++){
		if(sections[i].getAttribute('id')!=id){
			sections[i].style.display = 'none';
		}
		else{
			sections[i].style.display = 'block';
		}
	}
}

function prepareInternalnav() {
	if(!document.getElementById) return false;
	if(!document.getElementsByTagName) return false;

	var articles = document.getElementsByTagName('article');
	if(articles.length==0) return false;
	var navs = articles[0].getElementsByTagName('nav');
	if(navs.length==0) return false;

	var links = navs[0].getElementsByTagName('a');
	for(var i=0;i<links.length;i++){
		var sectionId = links[i].getAttribute('href').split("#")[1];
		if(!document.getElementById(sectionId)) continue;
		document.getElementById(sectionId).style.display='none';
		links[i].destination = sectionId;
		links[i].onclick = function(){
			showSection(this.destination);
			return false;
		}
	}
}

Racer.addLoadEvent(prepareInternalnav);


function prepareGallery () {
	if(!document.getElementById) return false;
	if(!document.getElementsByTagName) return false;

	if(!document.getElementById('imagegallery')) return false;
	var gallery = document.getElementById('imagegallery');
	var links = gallery.getElementsByTagName('a');
	for(var i=0;i<links.length;i++){
		links[i].onclick = function(){
			return showPic(this);
		}
	}
}

function preparePlaceholder () {
	if(!document.createElement) return false;
	if(!document.createTextNode) return false;
	if(!document.getElementById) return false;
	if(!document.getElementById('imagegallery')) return false;

	var placeholder = document.createElement('img');
	placeholder.setAttribute('id','placeholder');
	placeholder.setAttribute('src','images/placeholder.gif');
	placeholder.setAttribute('alt','my image gallery');
	var description = document.createElement('p');
	description.setAttribute('id','description');
	var desctext = document.createTextNode("Choose an image");
	description.appendChild(desctext);
	var gallery = document.getElementById('imagegallery');
	Racer.DOMHelper.insertAfter(description,gallery);
	Racer.DOMHelper.insertAfter(placeholder,description);
}

function showPic (whichpic) {
	if(!document.getElementById('placeholder')) return false;
	var source = whichpic.getAttribute('href');
	var placeholder = document.getElementById('placeholder');
	placeholder.setAttribute('src',source);
	if(!document.getElementById('description')) return false;

	var text;
	if(whichpic.getAttribute('title')){
		text = whichpic.getAttribute("title");
	}else{
		text="";
	}
	var description = document.getElementById('description');
	if(description.firstChild.nodeType==3){
		description.firstChild.nodeValue=text;
	}
	return false;
}

Racer.addLoadEvent(preparePlaceholder);
Racer.addLoadEvent(prepareGallery);