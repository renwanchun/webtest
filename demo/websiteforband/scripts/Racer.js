﻿var Racer = window.Racer || {};

//使用Object创建namespace
/*
 * Example 1:Racer.namespace('com')
 * Example 2:Racer.namespace('com.racer.common')
*/
Racer.namespace = function (mixed) {
  var argType = typeof (mixed);
  var strArr;
  if (argType == 'array') {
    strArr = mixed;
  }
  else if (argType == 'string') {
    strArr = mixed.split(".");
  }
  else {
    throw 'unsupport argument type';
  }
   
  var nsArr = new Array();
  nsArr.push(window);
  for (var i = 0; i < strArr.length; i++) {
    nsArr[i][strArr[i]] = nsArr[i][strArr[i]] || {};
    nsArr.push(nsArr[i][strArr[i]]);
  }
}

/*
 * @desc 在DOM加载完成之后,执行传入的函数.扩展了window.onload函数，支持多个函数的加载
 * @param func 要执行的函数
 */
Racer.addLoadEvent = function(func){
	var oldonload = window.onload;
  if(typeof window.onload != 'function'){
    window.onload = func;
  }
  else{
    window.onload = function(){
      oldonload();
      func();
    }
  }
}

/*
 * @desc DOM操作的辅助函数
**/
Racer.namespace('Racer.DOMHelper');

/*
 * @desc 获取下一个元素(区别nextSibling属性)
 * @param node 要获取下一元素的节点
 * @return 返回下一个元素节点;如果没有返回null
**/
Racer.DOMHelper.getNextElement = function(node){
  if(node.nextSibling){
    if(node.nextSibling.nodeType==1){
      return node.nextSibling;
    }
    return getNextElement(node.nextSibling);
  }
  return null;
}

Racer.DOMHelper.insertAfter=function(newElement,targetElement){
  var parent = targetElement.parentNode;
  if(parent.lastChild == targetElement){
    parent.appendChild(newElement);
  }
  else{
    parent.insertBefore(newElement,targetElement.nextSibling);
  }
}

Racer.DOMHelper.addClass=function(element,value){
  if(!element.className){
    element.className = value;
  }else{
    var newClassName = element.className;
    newClassName+=" ";
    newClassName+=value;
    element.className = newClassName;
  }
}


/*
 * @desc 与动画相关的函数
 */
Racer.namespace('Racer.Automation');

/*
 * @desc 移动元素到指定位置
 * @param elementId 要移动元素的id
 * @param final_x 目标位置的left值
 * @param final_y 目标位置的top值
 * @param interval 每个interval时间移动剩余距离的10分之1
 */
Racer.Automation.moveElement = function(elementId, final_x, final_y, interval) {
  if (!document.getElementById) return false;
  if (!document.getElementById(elementId)) return false;

  var elem = document.getElementById(elementId);
  if(elem.movement){
    clearTimeout(elem.movement);
  }

  if(!elem.style.left){
    elem.style.left='0px';
  }
    
  if(!elem.style.top){
    elem.style.top='0px';
  }
    
  var xpos = parseInt(elem.style.left);
  var ypos = parseInt(elem.style.top);
  var dist = 0;
  if (xpos == final_x && ypos == final_y) {
    return true;
  }
  if (xpos < final_x) {
    dist = Math.ceil((final_x-xpos)/10);
    xpos+=dist;
  }
  if (xpos > final_x) {
    dist = Math.ceil((xpos-final_x)/10);
    xpos-=dist;
  }
  if (ypos < final_y) {
    dist = Math.ceil((final_y-ypos)/10);
    ypos+=dist;
  }
  if (ypos > final_y) {
    dist = Math.ceil((ypos-final_y)/10);
    ypos-=dist;
  }
  elem.style.left = xpos + "px";
  elem.style.top = ypos + "px";

  var repeat = "Racer.Automation.moveElement('" + elementId + "'," + final_x + "," + final_y + "," + interval + ")";
  elem.movement = setTimeout(repeat, interval);
}