﻿//1.通过函数创建namespace,其缺点在于必须通过新建对象才能对函数进行调用

/*
var Namespace = window.Namespace || {};

Namespace.Hello = function () {
  this.name = "world";
}

Namespace.Hello.prototype.sayHello = function (_name) {
  return "Hello " + (_name || this.name);
}
*/

//2.通过函数创建namespace的改进

/*
var Namespace = window.Namespace || {};
Namespace.Hello = new function () {
  var _self = this;
  var name = "world";
  _self.sayHello = function (_name) {
    return "Hello " + (_name || name);
  }
}
*/



//3.通过JSON对象创建Object，其缺点在于所有的变量都是公有的

/*
var Namespace = window.Namespace || {};
Namespace.Hello = {
  name: 'world',
  sayHello: function (_name) {
    return 'Hello ' + (_name || this.name);
  }
}
*/


//4.通过闭包(Closure)和Object实现

/*
var Namespace = window.Namespace || {};
Namespace.Hello = (function () {
  //待返回的公有对象
  var _self = {};
  //私有对象和方法
  var __name = "world";
  //公有对象和方法
  _self.sayHello = function (_name) {
    return "Hello " + (_name || __name);
  }
  //返回公有对象
  return _self;
} ());
*/

//5.闭包(Closure)和Object的改进
var Namespace = window.Namespace || {};
Namespace.Hello = (function () {
  var __name = 'world';
  var sayHello = function (_name) {
    return "Hello " + (_name || __name);
  }

  //返回公有成员
  return {
    sayHello:sayHello
  }
} ());